//
//  MainTabCell.swift
//  DummyForHaze
//
//  Created by Rosy on 11/13/20.
//

import Foundation
import UIKit

class MainTabCell: UICollectionViewCell {
    
    lazy var myContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var tabLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var bottomSeparator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        create()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func create() {
        contentView.addSubview(myContentView)
        myContentView.addSubview(tabLabel)
//        myContentView.addSubview(bottomSeparator)
        NSLayoutConstraint.activate([
            
            myContentView.topAnchor.constraint(equalTo: contentView.topAnchor),
            myContentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            myContentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            myContentView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            tabLabel.centerYAnchor.constraint(equalTo: myContentView.centerYAnchor),
            tabLabel.leadingAnchor.constraint(equalTo: myContentView.leadingAnchor, constant: 20),
            tabLabel.trailingAnchor.constraint(equalTo: myContentView.trailingAnchor),
            
            tabLabel.bottomAnchor.constraint(equalTo: myContentView.bottomAnchor)

        ])
    }
    
    func setCell(text: String) {
        tabLabel.text = text
    }
}
