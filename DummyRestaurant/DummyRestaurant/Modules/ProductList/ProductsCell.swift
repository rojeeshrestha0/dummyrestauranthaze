//
//  ProductsCell.swift
//  DummyForHaze
//
//  Created by Rosy on 11/13/20.
//

import Foundation
import UIKit

class ProductsCell: UICollectionViewCell {
    
    lazy var myContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    ///image view for th eproduct image
    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        return view
    }()
    
    ///product name label that is below image
    lazy var productNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .darkGray
        label.textAlignment = .left
        return label
    }()
    
    ///price label below the product name
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .darkGray
        label.textAlignment = .left
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        create()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// to create the portion that contains the product collection view and the tab
    private func create() {
        contentView.addSubview(myContentView)
        myContentView.addSubview(imageView)
        myContentView.addSubview(productNameLabel)
        myContentView.addSubview(priceLabel)
        NSLayoutConstraint.activate([
            
            myContentView.topAnchor.constraint(equalTo: contentView.topAnchor),
            myContentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            myContentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            myContentView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            imageView.topAnchor.constraint(equalTo: myContentView.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: myContentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: myContentView.trailingAnchor),
            
            productNameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 3),
            productNameLabel.leadingAnchor.constraint(equalTo: myContentView.leadingAnchor, constant: 1),
            
            priceLabel.topAnchor.constraint(equalTo: productNameLabel.bottomAnchor, constant: 3),
            priceLabel.leadingAnchor.constraint(equalTo: productNameLabel.leadingAnchor),
            priceLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1)

        ])
    }
    
    func setCell(text: String) {
        productNameLabel.text = text
    }
}
