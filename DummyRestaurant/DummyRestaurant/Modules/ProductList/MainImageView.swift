//
//  MainImageView.swift
//  DummyForHaze
//
//  Created by Rosy on 11/12/20.
//


import Foundation
import UIKit

class MainImageView: UIView {
    
    ///image view
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    /// collection view
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        // layout.itemSize.height = 10
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(MainTabCell.self, forCellWithReuseIdentifier: MainTabCell.identifier)
        collectionView.backgroundColor = .darkGray
        collectionView.layer.borderWidth = 1
        collectionView.layer.borderColor = UIColor.systemGray.cgColor
        collectionView.accessibilityIdentifier = "MainTabCollection"
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    lazy var fieldContainerStack: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.spacing = 10.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    // MARK: - initializers
    init() {
        super.init(frame: .zero)
        create()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func create() {
        addSubview(fieldContainerStack)
        fieldContainerStack.addArrangedSubview(imageView)
        fieldContainerStack.addArrangedSubview(collectionView)
        
        NSLayoutConstraint.activate([
            // MARK: - first name
            fieldContainerStack.topAnchor.constraint(equalTo: topAnchor),
            fieldContainerStack.leadingAnchor.constraint(equalTo: leadingAnchor),
            fieldContainerStack.trailingAnchor.constraint(equalTo: trailingAnchor),
            fieldContainerStack.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            imageView.topAnchor.constraint(equalTo: fieldContainerStack.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: fieldContainerStack.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: fieldContainerStack.trailingAnchor),
            
            collectionView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 3),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
}
 
