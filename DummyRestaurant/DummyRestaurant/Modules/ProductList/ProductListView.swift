//
//  ProductListView.swift
//  DummyForHaze
//
//  Created by Rosy on 11/12/20.
//

import Foundation
import UIKit

class ProductListView: Baseview {
///image and tab view
    lazy var topImageView: MainImageView = {
        let view = MainImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var scrollView: UIScrollView = {
       let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    ///Product collection View
        lazy var productCollectionView: ProductsView = {
            let view = ProductsView()
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()

    override func createDesign() {
        super.createDesign()
        addSubview(topImageView)
        //addSubview(scrollView)
        addSubview(productCollectionView)
        
        NSLayoutConstraint.activate([
            topImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            topImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            topImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            productCollectionView.topAnchor.constraint(equalTo: topImageView.bottomAnchor, constant: 20),
            productCollectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            productCollectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            
           // scrollView.topAnchor.constraint(equalTo: <#T##NSLayoutAnchor<NSLayoutYAxisAnchor>#>)
            
        ])
    }
    
}
