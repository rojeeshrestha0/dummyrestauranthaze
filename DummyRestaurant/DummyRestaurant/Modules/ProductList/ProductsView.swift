//
//  ProductsView.swift
//  DummyForHaze
//
//  Created by Rosy on 11/12/20.
//

import Foundation
import UIKit

class ProductsView: UIView {
    
    ///image view
    /// searchBar
    lazy var searchField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Search"
        textField.textColor = .black
        textField.returnKeyType = .search
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .lightGray
        textField.returnKeyType = .search
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15.0, height: 30.0))
        textField.leftViewMode = .always
        textField.layer.cornerRadius = 15.0
        textField.layer.masksToBounds = true
        textField.font = UIFont.font(.montserrat, weight: .regular, ofSize: 14.0)
        return textField
    }()
    
    /// tabCollectionView
    lazy var tabcollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(MainTabCell.self, forCellWithReuseIdentifier: MainTabCell.identifier)
        collectionView.backgroundColor = .darkGray
        collectionView.layer.borderWidth = 1
        collectionView.layer.borderColor = UIColor.systemGray.cgColor
        collectionView.accessibilityIdentifier = "ProductTabCollection"
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    /// productCollectionview
    lazy var productCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(ProductsCell.self, forCellWithReuseIdentifier: ProductsCell.identifier)
        collectionView.backgroundColor = .darkGray
        collectionView.layer.borderWidth = 1
        collectionView.layer.borderColor = UIColor.systemGray.cgColor
        collectionView.accessibilityIdentifier = "ProductTabCollection"
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    
    // MARK: - initializers
    init() {
        super.init(frame: .zero)
        create()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func create() {
        addSubview(searchField)
        addSubview(tabcollectionView)
        addSubview(productCollectionView)
        
        NSLayoutConstraint.activate([
            searchField.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            searchField.leadingAnchor.constraint(equalTo: leadingAnchor),
            searchField.trailingAnchor.constraint(equalTo: trailingAnchor),
            searchField.heightAnchor.constraint(equalToConstant: 35.0),
            
            tabcollectionView.topAnchor.constraint(equalTo: searchField.bottomAnchor, constant: 5),
            tabcollectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tabcollectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            productCollectionView.topAnchor.constraint(equalTo: tabcollectionView.bottomAnchor, constant: 5),
            productCollectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            productCollectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            productCollectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
 
