//
//  ProductListController.swift
//  DummyForHaze
//
//  Created by Rosy on 11/12/20.
//

import Foundation
import Combine
import UIKit

class ProductListController: BaseController {
    
    var screenView: ProductListView {
        baseView as! ProductListView
    }
    
    var viewModel: ProductListViewModel {
        baseViewModel as!  ProductListViewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func observeEvent() {
    }
}
