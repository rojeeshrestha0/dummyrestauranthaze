//
//  DummyManager.swift
//  DummyForHaze
//
//  Created by Rosy on 11/13/20.
//

import Foundation
import Combine

typealias Paramters = [String: Any]
class DummyManager {
    var bag = Set<AnyCancellable>()
    @Published var dummyData: [DummyItem] = []
    let isApicall = PassthroughSubject<Bool,Never>()
    let networking = Networking()
    
    func request(router: Routable) {
        isApicall.send(true)
        networking.request(router: router).sink { [weak self] result in
            guard let self = self else {  return }
            self.isApicall.send(false)
            self.parseResult(result: result, router: router)
        }.store(in: &bag)
    }
    
    
    private func parseResult(result: NetworkingResult, router: Routable)  {
        switch router {
        case DummyRouter.list:
            if !result.object.isEmpty,
               let dummyJson = result.object["dummyItem"] as? [Paramters]
            {
                let dummyData = DataMapper<DummyItem>.arrayMapping(dummyJson)
                print(dummyJson)
                self.dummyData =  dummyData
            }
            break
        default:
            break
        }
        
    }
}
