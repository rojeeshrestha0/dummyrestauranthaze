//
//  DummyItem.swift
//  DummyForHaze
//
//  Created by Rosy on 11/13/20.
//

import Foundation
struct DummyItem: Codable {
    var cover_image: String
    var categories: [Categories]

    
    
    init(from decoder: Decoder) throws {
        let  container = try decoder.container(keyedBy: CodingKeys.self)
        cover_image =  try container.decodeIfPresent(String.self, forKey: .cover_image) ?? ""
        categories =  try container.decodeIfPresent([Categories].self, forKey: .categories) ?? [Categories()]
    }
}

struct Categories: Codable {
    var foodData: [FoodData]
    
    enum CodeKeys: String, CodingKey {
        case foodData
    }
    
    init() {
        foodData = []
    }
    
    init(from decoder: Decoder) throws {
        let  container = try decoder.container(keyedBy: CodeKeys.self)
        foodData =  try container.decodeIfPresent([FoodData].self, forKey: .foodData) ?? [FoodData()]
    }
}


struct FoodData: Codable {
    var id: Int
    var featured_image: String
    var price: Int
    var name: String
    
    enum CodeKeys: String, CodingKey {
        case id
        case featured_image
        case price
        case name
        
    }
    
    init() {
        id = 0
        featured_image = ""
        price = 0
        name = ""
    }
    
    init(from decoder: Decoder) throws {
        let  container = try decoder.container(keyedBy: CodeKeys.self)
        id =  try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        featured_image =  try container.decodeIfPresent(String.self, forKey: .featured_image) ?? ""
        price =  try container.decodeIfPresent(Int.self, forKey: .price) ?? 0
        name =  try container.decodeIfPresent(String.self, forKey: .name) ?? ""

    }
}
