//
//  DummyRouter.swift
//  DummyForHaze
//
//  Created by Rosy on 11/12/20.
//

import Foundation
import Alamofire
//typealias Parameters = [String: Any]


enum DummyRouter: Routable {
    
    case list
    
    var path: String {
        switch self {
        case .list:
            return "public/restaurant/branch/18"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: DummyRouter.baseUrl.appendingPathComponent(path))
        urlRequest.httpMethod = httpMethod.rawValue
        headers.forEach { urlRequest.addValue($0.value, forHTTPHeaderField: $0.key) }
        return urlRequest
    }
    
    
}

