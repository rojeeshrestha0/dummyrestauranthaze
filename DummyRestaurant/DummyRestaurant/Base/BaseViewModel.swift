//
//  BaseViewModel.swift
//  DummyForHaze
//
//  Created by Rosy on 11/12/20.
//

import Foundation
import Foundation
import Combine

class BaseViewModel {
    var bag = Set<AnyCancellable>()

    init() { }
}
